<?php
/**
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved
 */

namespace AliyunLog\Log\Models\Response;

/**
 * The response of the CreateSqlInstance API from log service.
 *
 * @author log service dev
 */
class UpdaetSqlInstanceResponse extends Response
{
    
    /**
     * CreateSqlInstanceResponse constructor
     *
     * @param array $resp
     *            CreateSqlInstance HTTP response body
     * @param array $header
     *            CreateSqlInstance HTTP response header
     */
    public function __construct($resp, $header)
    {
        parent::__construct($header);
    }
    
}
