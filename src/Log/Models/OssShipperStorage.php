<?php
/**
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved
 */

namespace AliyunLog\Log\Models;
class OssShipperStorage
{
    private $format;
    
    /**
     * @return mixed
     */
    public function getFormat()
    {
        return $this->format;
    }
    
    /**
     * @param mixed $format
     */
    public function setFormat($format)
    {
        $this->format = $format;
    }
    
    
}
