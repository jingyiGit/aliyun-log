<?php

namespace AliyunLog\Log;

use AliyunLog\Log\Models\Request\PutLogsRequest;
use AliyunLog\Log\Models\Response\PutLogsResponse;
use AliyunLog\Log\Models\Request\CreateShipperRequest;
use AliyunLog\Log\Models\Response\CreateShipperResponse;
use AliyunLog\Log\Models\Response\BatchGetLogsResponse;
use AliyunLog\Log\Models\Request\ListShardsRequest;
use AliyunLog\Log\Models\Response\ListShardsResponse;
use AliyunLog\Log\Models\Request\SplitShardRequest;
use AliyunLog\Log\Models\Request\MergeShardsRequest;
use AliyunLog\Log\Models\Request\DeleteShardRequest;
use AliyunLog\Log\Models\Response\DeleteShardResponse;
use AliyunLog\Log\Models\Request\GetCursorRequest;
use AliyunLog\Log\Models\Response\GetCursorResponse;
use AliyunLog\Log\Models\Request\CreateConfigRequest;
use AliyunLog\Log\Models\Request\UpdateConfigRequest;
use AliyunLog\Log\Models\Response\CreateConfigResponse;
use AliyunLog\Log\Models\Response\UpdateConfigResponse;
use AliyunLog\Log\Models\Request\GetConfigRequest;
use AliyunLog\Log\Models\Request\DeleteConfigRequest;
use AliyunLog\Log\Models\Response\GetConfigResponse;
use AliyunLog\Log\Models\Response\DeleteConfigResponse;
use AliyunLog\Log\Models\Request\ListConfigsRequest;
use AliyunLog\Log\Models\Response\ListConfigsResponse;
use AliyunLog\Log\Models\Request\CreateMachineGroupRequest;
use AliyunLog\Log\Models\Request\UpdateMachineGroupRequest;
use AliyunLog\Log\Models\Response\CreateMachineGroupResponse;
use AliyunLog\Log\Models\Request\ProjectSqlRequest;
use AliyunLog\Log\Models\Response\GetLogsResponse;
use AliyunLog\Log\Models\Request\GetProjectLogsRequest;
use AliyunLog\Log\Models\Response\ProjectSqlResponse;
use AliyunLog\Log\Models\Response\CreateSqlInstanceResponse;
use AliyunLog\Log\Models\Response\UpdateSqlInstanceResponse;
use AliyunLog\Log\Models\Response\ListSqlInstanceResponse;
use AliyunLog\Log\Models\Request\BatchGetLogsRequest;
use AliyunLog\Log\Models\Request\DeleteMachineGroupRequest;
use AliyunLog\Log\Models\Response\GetMachineGroupResponse;
use AliyunLog\Log\Models\Request\ListMachineGroupsRequest;
use AliyunLog\Log\Models\Response\DeleteMachineGroupResponse;
use AliyunLog\Log\Models\Response\ListMachineGroupsResponse;
use AliyunLog\Log\Models\Request\ApplyConfigToMachineGroupRequest;
use AliyunLog\Log\Models\Response\UpdateACLResponse;
use AliyunLog\Log\Models\Request\GetACLRequest;
use AliyunLog\Log\Models\Response\GetACLResponse;
use AliyunLog\Log\Models\Request\DeleteACLRequest;
use AliyunLog\Log\Models\Response\DeleteACLResponse;
use AliyunLog\Log\Models\Request\ListACLsRequest;
use AliyunLog\Log\Models\Response\ListACLsResponse;
use AliyunLog\Log\Models\Response\CreateACLResponse;
use AliyunLog\Log\Models\Request\UpdateACLRequest;
use AliyunLog\Log\Models\Request\CreateACLRequest;
use AliyunLog\Log\Models\Response\GetMachineResponse;
use AliyunLog\Log\Models\Response\RemoveConfigFromMachineGroupResponse;
use AliyunLog\Log\Models\Request\RemoveConfigFromMachineGroupRequest;
use AliyunLog\Log\Models\Response\ApplyConfigToMachineGroupResponse;
use AliyunLog\Log\Models\Request\GetMachineRequest;
use AliyunLog\Log\Models\Response\LogStoreSqlResponse;
use AliyunLog\Log\Models\Request\LogStoreSqlRequest;
use AliyunLog\Log\Models\Request\UpdateShipperRequest;
use AliyunLog\Log\Models\Response\UpdateShipperResponse;
use AliyunLog\Log\Models\Request\GetShipperTasksRequest;
use AliyunLog\Log\Models\Response\GetShipperTasksResponse;
use AliyunLog\Log\Models\Request\RetryShipperTasksRequest;
use AliyunLog\Log\Models\Response\RetryShipperTasksResponse;

/**
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved
 */
date_default_timezone_set('Asia/Shanghai');

if (!defined('ALIYUN_LOG_API_VERSION')) {
    define('ALIYUN_LOG_API_VERSION', '0.6.0');
}
if (!defined('ALIYUN_LOG_USER_AGENT'))
    define('ALIYUN_LOG_USER_AGENT', 'log-php-sdk-v-0.6.0');

/**
 * Client class is the main class in the SDK. It can be used to
 * communicate with LOG server to put/get data.
 *
 * @author log_dev
 */
class Client
{
    
    /**
     * @var string aliyun accessKey
     */
    protected $accessKey;
    
    /**
     * @var string aliyun accessKeyId
     */
    protected $accessKeyId;
    
    /**
     * @var string aliyun sts token
     */
    protected $stsToken;
    
    /**
     * @var string LOG endpoint
     */
    protected $endpoint;
    
    /**
     * @var string Check if the host if row ip.
     */
    protected $isRowIp;
    
    /**
     * @var integer Http send port. The dafault value is 80.
     */
    protected $port;
    
    /**
     * @var string log sever host.
     */
    protected $logHost;
    
    /**
     * @var string the local machine ip address.
     */
    protected $source;
    
    /**
     * Client constructor
     *
     * @param string $endpoint
     *            LOG host name, for example, http://cn-hangzhou.sls.aliyuncs.com
     * @param string $accessKeyId
     *            aliyun accessKeyId
     * @param string $accessKey
     *            aliyun accessKey
     */
    public function __construct($endpoint, $accessKeyId, $accessKey, $token = "")
    {
        $this->setEndpoint($endpoint); // set $this->logHost
        $this->accessKeyId = $accessKeyId;
        $this->accessKey   = $accessKey;
        $this->stsToken    = $token;
        $this->source      = Util::getLocalIp();
    }
    
    private function setEndpoint($endpoint)
    {
        $pos = strpos($endpoint, "://");
        if ($pos !== false) { // be careful, !==
            $pos      += 3;
            $endpoint = substr($endpoint, $pos);
        }
        $pos = strpos($endpoint, "/");
        if ($pos !== false) // be careful, !==
            $endpoint = substr($endpoint, 0, $pos);
        $pos = strpos($endpoint, ':');
        if ($pos !== false) { // be careful, !==
            $this->port = ( int )substr($endpoint, $pos + 1);
            $endpoint   = substr($endpoint, 0, $pos);
        } else
            $this->port = 80;
        $this->isRowIp  = Util::isIp($endpoint);
        $this->logHost  = $endpoint;
        $this->endpoint = $endpoint . ':' . ( string )$this->port;
    }
    
    /**
     * GMT format time string.
     *
     * @return string
     */
    protected function getGMT()
    {
        return gmdate('D, d M Y H:i:s') . ' GMT';
    }
    
    
    /**
     * Decodes a JSON string to a JSON Object.
     * Unsuccessful decode will cause an AliyunException.
     *
     * @return string
     * @throws AliyunException
     */
    protected function parseToJson($resBody, $requestId)
    {
        if (!$resBody)
            return NULL;
        
        $result = json_decode($resBody, true);
        if ($result === NULL) {
            throw new AliyunException ('BadResponse', "Bad format,not json: $resBody", $requestId);
        }
        return $result;
    }
    
    /**
     * @param $method
     * @param $url
     * @param $body
     * @param $headers
     * @return array
     * @throws RequestCore_Exception
     */
    protected function getHttpResponse($method, $url, $body, $headers)
    {
        $request = new RequestCore ($url);
        foreach ($headers as $key => $value) {
            $request->add_header($key, $value);
        }
        $request->set_method($method);
        $request->set_useragent(ALIYUN_LOG_USER_AGENT);
        if ($method == "POST" || $method == "PUT") {
            $request->set_body($body);
        }
        
        $request->send_request();
        $response    = [];
        $response [] = ( int )$request->get_response_code();
        $response [] = $request->get_response_header();
        $response [] = $request->get_response_body();
        return $response;
    }
    
    /**
     * @return array
     * @throws AliyunException
     */
    private function sendRequest($method, $url, $body, $headers)
    {
        try {
            [$responseCode, $header, $resBody] = $this->getHttpResponse($method, $url, $body, $headers);
        } catch (\Exception $ex) {
            throw new AliyunException ($ex->getMessage(), $ex->__toString());
        }
        $requestId = isset ($header ['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        
        if ($responseCode == 200) {
            return [$resBody, $header];
        } else {
            $exJson = $this->parseToJson($resBody, $requestId);
            if (isset($exJson ['error_code']) && isset($exJson ['error_message'])) {
                throw new AliyunException ($exJson ['error_code'],
                    $exJson ['error_message'], $requestId);
            } else {
                if ($exJson) {
                    $exJson = ' The return json is ' . json_encode($exJson);
                } else {
                    $exJson = '';
                }
                throw new AliyunException ('RequestError',
                    "Request is failed. Http code is $responseCode.$exJson", $requestId);
            }
        }
    }
    
    /**
     * @return array
     * @throws AliyunException
     */
    private function send($method, $project, $body, $resource, $params, $headers)
    {
        if ($body) {
            $headers ['Content-Length'] = strlen($body);
            if (!isset($headers ["x-log-bodyrawsize"]))
                $headers ["x-log-bodyrawsize"] = 0;
            $headers ['Content-MD5'] = Util::calMD5($body);
        } else {
            $headers ['Content-Length']    = 0;
            $headers ["x-log-bodyrawsize"] = 0;
            $headers ['Content-Type']      = ''; // If not set, http request will add automatically.
        }
        
        $headers ['x-log-apiversion']      = ALIYUN_LOG_API_VERSION;
        $headers ['x-log-signaturemethod'] = 'hmac-sha1';
        if (strlen($this->stsToken) > 0)
            $headers ['x-acs-security-token'] = $this->stsToken;
        if (is_null($project)) $headers ['Host'] = $this->logHost;
        else $headers ['Host'] = "$project.$this->logHost";
        $headers ['Date']          = $this->GetGMT();
        $signature                 = Util::getRequestAuthorization($method, $resource, $this->accessKey, $this->stsToken, $params, $headers);
        $headers ['Authorization'] = "LOG $this->accessKeyId:$signature";
        
        $url = $resource;
        if ($params)
            $url .= '?' . Util::urlEncode($params);
        if ($this->isRowIp)
            $url = "http://$this->endpoint$url";
        else {
            if (is_null($project))
                $url = "http://$this->endpoint$url";
            else  $url = "http://$project.$this->endpoint$url";
        }
        return $this->sendRequest($method, $url, $body, $headers);
    }
    
    /**
     * Put logs to Log Service.
     * Unsuccessful opertaion will cause an AliyunException.
     *
     * @param PutLogsRequest $request the PutLogs request parameters class
     * @return PutLogsResponse
     * @throws AliyunException
     */
    public function putLogs(PutLogsRequest $request)
    {
        if (count($request->getLogitems()) > 4096)
            throw new AliyunException ('InvalidLogSize', "logItems' length exceeds maximum limitation: 4096 lines.");
        
        $logGroup = new SlsLogGroup ();
        $topic    = $request->getTopic() !== null ? $request->getTopic() : '';
        $logGroup->setTopic($request->getTopic());
        $source = $request->getSource();
        
        if (!$source)
            $source = $this->source;
        $logGroup->setSource($source);
        $logitems = $request->getLogitems();
        foreach ($logitems as $logItem) {
            $log = new SlsLog ();
            $log->setTime($logItem->getTime());
            $content = $logItem->getContents();
            foreach ($content as $key => $value) {
                $content = new SlsLogContent ();
                $content->setKey($key);
                $content->setValue($value);
                $log->addContents($content);
            }
            
            $logGroup->addLogs($log);
        }
        
        $body = Util::toBytes($logGroup);
        unset ($logGroup);
        
        $bodySize = strlen($body);
        if ($bodySize > 3 * 1024 * 1024) // 3 MB
            throw new AliyunException ('InvalidLogSize', "logItems' size exceeds maximum limitation: 3 MB.");
        $params                         = [];
        $headers                        = [];
        $headers ["x-log-bodyrawsize"]  = $bodySize;
        $headers ['x-log-compresstype'] = 'deflate';
        $headers ['Content-Type']       = 'application/x-protobuf';
        $body                           = gzcompress($body, 6);
        
        $logstore = $request->getLogstore() !== null ? $request->getLogstore() : '';
        $project  = $request->getProject() !== null ? $request->getProject() : '';
        $shardKey = $request->getShardKey();
        $resource = "/logstores/" . $logstore . ($shardKey == null ? "/shards/lb" : "/shards/route");
        if ($shardKey) {
            $params["key"] = $shardKey;
        }
        
        [$resp, $header] = $this->send("POST", $project, $body, $resource, $params, $headers);
        $requestId = isset ($header ['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return new PutLogsResponse ($header);
    }
    
    /**
     * create shipper service
     *
     * @param CreateShipperRequest $request
     * return CreateShipperResponse
     * @return CreateShipperResponse
     * @throws AliyunException
     */
    public function createShipper(CreateShipperRequest $request)
    {
        $headers                 = [];
        $params                  = [];
        $resource                = "/logstores/" . $request->getLogStore() . "/shipper";
        $project                 = $request->getProject() !== null ? $request->getProject() : '';
        $headers["Content-Type"] = "application/json";
        
        $body                         = [
            "shipperName"         => $request->getShipperName(),
            "targetType"          => $request->getTargetType(),
            "targetConfiguration" => $request->getTargetConfigration(),
        ];
        $body_str                     = json_encode($body);
        $headers["x-log-bodyrawsize"] = strlen($body_str);
        [$resp, $header] = $this->send("POST", $project, $body_str, $resource, $params, $headers);
        $requestId = isset($header['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return new CreateShipperResponse($resp, $header);
    }
    
    /**
     * create shipper service
     *
     * @param UpdateShipperRequest $request
     * return UpdateShipperResponse
     * @return UpdateShipperResponse
     * @throws AliyunException
     */
    public function updateShipper(UpdateShipperRequest $request)
    {
        $headers                 = [];
        $params                  = [];
        $resource                = "/logstores/" . $request->getLogStore() . "/shipper/" . $request->getShipperName();
        $project                 = $request->getProject() !== null ? $request->getProject() : '';
        $headers["Content-Type"] = "application/json";
        
        $body                         = [
            "shipperName"         => $request->getShipperName(),
            "targetType"          => $request->getTargetType(),
            "targetConfiguration" => $request->getTargetConfigration(),
        ];
        $body_str                     = json_encode($body);
        $headers["x-log-bodyrawsize"] = strlen($body_str);
        [$resp, $header] = $this->send("PUT", $project, $body_str, $resource, $params, $headers);
        $requestId = isset($header['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return new UpdateShipperResponse($resp, $header);
    }
    
    /**
     * get shipper tasks list, max 48 hours duration supported
     *
     * @param GetShipperTasksRequest $request
     * return GetShipperTasksResponse
     * @return GetShipperTasksResponse
     * @throws AliyunException
     */
    public function getShipperTasks(GetShipperTasksRequest $request)
    {
        $headers                      = [];
        $params                       = [
            'from'   => $request->getStartTime(),
            'to'     => $request->getEndTime(),
            'status' => $request->getStatusType(),
            'offset' => $request->getOffset(),
            'size'   => $request->getSize(),
        ];
        $resource                     = "/logstores/" . $request->getLogStore() . "/shipper/" . $request->getShipperName() . "/tasks";
        $project                      = $request->getProject() !== null ? $request->getProject() : '';
        $headers["x-log-bodyrawsize"] = 0;
        $headers["Content-Type"]      = "application/json";
        
        [$resp, $header] = $this->send("GET", $project, null, $resource, $params, $headers);
        $requestId = isset($header['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return new GetShipperTasksResponse($resp, $header);
    }
    
    /**
     * retry shipper tasks list by task ids
     *
     * @param RetryShipperTasksRequest $request
     * return RetryShipperTasksResponse
     * @return RetryShipperTasksResponse
     * @throws AliyunException
     */
    public function retryShipperTasks(RetryShipperTasksRequest $request)
    {
        $headers  = [];
        $params   = [];
        $resource = "/logstores/" . $request->getLogStore() . "/shipper/" . $request->getShipperName() . "/tasks";
        $project  = $request->getProject() !== null ? $request->getProject() : '';
        
        $headers["Content-Type"]      = "application/json";
        $body                         = $request->getTaskLists();
        $body_str                     = json_encode($body);
        $headers["x-log-bodyrawsize"] = strlen($body_str);
        [$resp, $header] = $this->send("PUT", $project, $body_str, $resource, $params, $headers);
        $requestId = isset($header['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return new RetryShipperTasksResponse($resp, $header);
    }
    
    /**
     * delete shipper service
     *
     * @param DeleteShipperRequest $request
     * return DeleteShipperResponse
     */
    public function deleteShipper(DeleteShipperRequest $request)
    {
        $headers                      = [];
        $params                       = [];
        $resource                     = "/logstores/" . $request->getLogStore() . "/shipper/" . $request->getShipperName();
        $project                      = $request->getProject() !== null ? $request->getProject() : '';
        $headers["x-log-bodyrawsize"] = 0;
        $headers["Content-Type"]      = "application/json";
        
        [$resp, $header] = $this->send("DELETE", $project, null, $resource, $params, $headers);
        $requestId = isset($header['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return new DeleteShipperResponse($resp, $header);
    }
    
    /**
     * get shipper config service
     *
     * @param GetShipperConfigRequest $request
     * return GetShipperConfigResponse
     */
    public function getShipperConfig(GetShipperConfigRequest $request)
    {
        $headers                      = [];
        $params                       = [];
        $resource                     = "/logstores/" . $request->getLogStore() . "/shipper/" . $request->getShipperName();
        $project                      = $request->getProject() !== null ? $request->getProject() : '';
        $headers["x-log-bodyrawsize"] = 0;
        $headers["Content-Type"]      = "application/json";
        
        [$resp, $header] = $this->send("GET", $project, null, $resource, $params, $headers);
        $requestId = isset($header['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return new GetShipperConfigResponse($resp, $header);
    }
    
    /**
     * list shipper service
     *
     * @param ListShipperRequest $request
     * return ListShipperResponse
     */
    public function listShipper(ListShipperRequest $request)
    {
        $headers                      = [];
        $params                       = [];
        $resource                     = "/logstores/" . $request->getLogStore() . "/shipper";
        $project                      = $request->getProject() !== null ? $request->getProject() : '';
        $headers["x-log-bodyrawsize"] = 0;
        $headers["Content-Type"]      = "application/json";
        
        [$resp, $header] = $this->send("GET", $project, null, $resource, $params, $headers);
        $requestId = isset($header['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return new ListShipperResponse($resp, $header);
    }
    
    /**
     * create logstore
     * Unsuccessful opertaion will cause an AliyunException.
     *
     * @param CreateLogstoreRequest $request                   the CreateLogStore request parameters class.
     * @throws AliyunException
     *                                                         return CreateLogstoreResponse
     */
    public function createLogstore(CreateLogstoreRequest $request)
    {
        $headers                      = [];
        $params                       = [];
        $resource                     = '/logstores';
        $project                      = $request->getProject() !== null ? $request->getProject() : '';
        $headers["x-log-bodyrawsize"] = 0;
        $headers["Content-Type"]      = "application/json";
        $body                         = [
            "logstoreName" => $request->getLogstore(),
            "ttl"          => (int)($request->getTtl()),
            "shardCount"   => (int)($request->getShardCount()),
        ];
        $body_str                     = json_encode($body);
        $headers["x-log-bodyrawsize"] = strlen($body_str);
        [$resp, $header] = $this->send("POST", $project, $body_str, $resource, $params, $headers);
        $requestId = isset ($header ['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return new CreateLogstoreResponse($resp, $header);
    }
    
    /**
     * update logstore
     * Unsuccessful opertaion will cause an AliyunException.
     *
     * @param UpdateLogstoreRequest $request                   the UpdateLogStore request parameters class.
     * @throws AliyunException
     *                                                         return UpdateLogstoreResponse
     */
    public function updateLogstore(UpdateLogstoreRequest $request)
    {
        $headers                      = [];
        $params                       = [];
        $project                      = $request->getProject() !== null ? $request->getProject() : '';
        $headers["Content-Type"]      = "application/json";
        $body                         = [
            "logstoreName" => $request->getLogstore(),
            "ttl"          => (int)($request->getTtl()),
            "shardCount"   => (int)($request->getShardCount()),
        ];
        $resource                     = '/logstores/' . $request->getLogstore();
        $body_str                     = json_encode($body);
        $headers["x-log-bodyrawsize"] = strlen($body_str);
        [$resp, $header] = $this->send("PUT", $project, $body_str, $resource, $params, $headers);
        $requestId = isset ($header ['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return new UpdateLogstoreResponse($resp, $header);
    }
    
    /**
     * List all logstores of requested project.
     * Unsuccessful opertaion will cause an AliyunException.
     *
     * @param ListLogstoresRequest $request the ListLogstores request parameters class.
     * @return ListLogstoresResponse
     * @throws AliyunException
     */
    public function listLogstores(ListLogstoresRequest $request)
    {
        $headers  = [];
        $params   = [];
        $resource = '/logstores';
        $project  = $request->getProject() !== null ? $request->getProject() : '';
        [$resp, $header] = $this->send("GET", $project, NULL, $resource, $params, $headers);
        $requestId = isset ($header ['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return new ListLogstoresResponse ($resp, $header);
    }
    
    /**
     * Delete logstore
     * Unsuccessful opertaion will cause an AliyunException.
     *
     * @param DeleteLogstoreRequest $request the DeleteLogstores request parameters class.
     * @return DeleteLogstoresResponse
     * @throws AliyunException
     */
    public function deleteLogstore(DeleteLogstoreRequest $request)
    {
        $headers  = [];
        $params   = [];
        $project  = $request->getProject() !== null ? $request->getProject() : '';
        $logstore = $request->getLogstore() != null ? $request->getLogstore() : "";
        $resource = "/logstores/$logstore";
        [$resp, $header] = $this->send("DELETE", $project, NULL, $resource, $params, $headers);
        $requestId = isset ($header ['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return new DeleteLogstoreResponse ($resp, $header);
    }
    
    /**
     * List all topics in a logstore.
     * Unsuccessful opertaion will cause an AliyunException.
     *
     * @param ListTopicsRequest $request the ListTopics request parameters class.
     * @return ListTopicsResponse
     * @throws AliyunException
     */
    public function listTopics(ListTopicsRequest $request)
    {
        $headers = [];
        $params  = [];
        if ($request->getToken() !== null)
            $params ['token'] = $request->getToken();
        if ($request->getLine() !== null)
            $params ['line'] = $request->getLine();
        $params ['type'] = 'topic';
        $logstore        = $request->getLogstore() !== null ? $request->getLogstore() : '';
        $project         = $request->getProject() !== null ? $request->getProject() : '';
        $resource        = "/logstores/$logstore";
        [$resp, $header] = $this->send("GET", $project, NULL, $resource, $params, $headers);
        $requestId = isset ($header ['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return new ListTopicsResponse ($resp, $header);
    }
    
    /**
     * Get histograms of requested query from log service.
     * Unsuccessful opertaion will cause an AliyunException.
     *
     * @param GetHistogramsRequest $request the GetHistograms request parameters class.
     * @return array(json body, http header)
     * @throws AliyunException
     */
    public function getHistogramsJson(GetHistogramsRequest $request)
    {
        $headers = [];
        $params  = [];
        if ($request->getTopic() !== null)
            $params ['topic'] = $request->getTopic();
        if ($request->getFrom() !== null)
            $params ['from'] = $request->getFrom();
        if ($request->getTo() !== null)
            $params ['to'] = $request->getTo();
        if ($request->getQuery() !== null)
            $params ['query'] = $request->getQuery();
        $params ['type'] = 'histogram';
        $logstore        = $request->getLogstore() !== null ? $request->getLogstore() : '';
        $project         = $request->getProject() !== null ? $request->getProject() : '';
        $resource        = "/logstores/$logstore";
        [$resp, $header] = $this->send("GET", $project, NULL, $resource, $params, $headers);
        $requestId = isset ($header ['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return [$resp, $header];
    }
    
    /**
     * Get histograms of requested query from log service.
     * Unsuccessful opertaion will cause an AliyunException.
     *
     * @param GetHistogramsRequest $request the GetHistograms request parameters class.
     * @return GetHistogramsResponse
     * @throws AliyunException
     */
    public function getHistograms(GetHistogramsRequest $request)
    {
        $ret    = $this->getHistogramsJson($request);
        $resp   = $ret[0];
        $header = $ret[1];
        return new GetHistogramsResponse ($resp, $header);
    }
    
    /**
     * Get logs from Log service.
     * Unsuccessful opertaion will cause an AliyunException.
     *
     * @param GetLogsRequest $request the GetLogs request parameters class.
     * @return array(json body, http header)
     * @throws AliyunException
     */
    public function getLogsJson(GetLogsRequest $request)
    {
        $headers = [];
        $params  = [];
        if ($request->getTopic() !== null)
            $params ['topic'] = $request->getTopic();
        if ($request->getFrom() !== null)
            $params ['from'] = $request->getFrom();
        if ($request->getTo() !== null)
            $params ['to'] = $request->getTo();
        if ($request->getQuery() !== null)
            $params ['query'] = $request->getQuery();
        $params ['type'] = 'log';
        if ($request->getLine() !== null)
            $params ['line'] = $request->getLine();
        if ($request->getOffset() !== null)
            $params ['offset'] = $request->getOffset();
        if ($request->getOffset() !== null)
            $params ['reverse'] = $request->getReverse() ? 'true' : 'false';
        if ($request->getPowerSql() != null)
            $params ["powerSql"] = $request->getPowerSql() ? 'true' : 'false';
        $logstore = $request->getLogstore() !== null ? $request->getLogstore() : '';
        $project  = $request->getProject() !== null ? $request->getProject() : '';
        $resource = "/logstores/$logstore";
        [$resp, $header] = $this->send("GET", $project, NULL, $resource, $params, $headers);
        $requestId = isset ($header ['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return [$resp, $header];
        //return new GetLogsResponse ( $resp, $header );
    }
    
    /**
     * Get logs from Log service.
     * Unsuccessful opertaion will cause an AliyunException.
     *
     * @param GetLogsRequest $request the GetLogs request parameters class.
     * @return GetLogsResponse
     * @throws AliyunException
     */
    public function getLogs(GetLogsRequest $request)
    {
        $ret    = $this->getLogsJson($request);
        $resp   = $ret[0];
        $header = $ret[1];
        return new GetLogsResponse ($resp, $header);
    }
    
    /**
     * Get logs from Log service.
     * Unsuccessful opertaion will cause an AliyunException.
     *
     * @param GetProjectLogsRequest $request the GetLogs request parameters class.
     * @return array(json body, http header)
     * @throws AliyunException
     */
    public function getProjectLogsJson(GetProjectLogsRequest $request)
    {
        $headers = [];
        $params  = [];
        if ($request->getQuery() !== null)
            $params ['query'] = $request->getQuery();
        if ($request->getPowerSql() != null)
            $params ["powerSql"] = $request->getPowerSql() ? 'true' : 'false';
        $project  = $request->getProject() !== null ? $request->getProject() : '';
        $resource = "/logs";
        [$resp, $header] = $this->send("GET", $project, NULL, $resource, $params, $headers);
        $requestId = isset ($header ['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return [$resp, $header];
        //return new GetLogsResponse ( $resp, $header );
    }
    
    /**
     * Get logs from Log service.
     * Unsuccessful opertaion will cause an AliyunException.
     *
     * @param GetProjectLogsRequest $request the GetLogs request parameters class.
     * @return GetLogsResponse
     * @throws AliyunException
     */
    public function getProjectLogs(GetProjectLogsRequest $request)
    {
        $ret    = $this->getProjectLogsJson($request);
        $resp   = $ret[0];
        $header = $ret[1];
        return new GetLogsResponse ($resp, $header);
    }
    
    /**
     * execute sql on logstore
     * Unsuccessful opertaion will cause an AliyunException.
     *
     * @param LogStoreSqlRequest $request the executeLogStoreSql request parameters class
     * @return LogStoreSqlResponse
     * @throws AliyunException
     */
    public function executeLogStoreSql(LogStoreSqlRequest $request)
    {
        $headers = [];
        $params  = [];
        if ($request->getFrom() !== null)
            $params ['from'] = $request->getFrom();
        if ($request->getTo() !== null)
            $params ['to'] = $request->getTo();
        if ($request->getQuery() !== null)
            $params ['query'] = $request->getQuery();
        $params ['type'] = 'log';
        if ($request->getPowerSql() != null)
            $params ["powerSql"] = $request->getPowerSql() ? 'true' : 'false';
        $logstore = $request->getLogstore() !== null ? $request->getLogstore() : '';
        $project  = $request->getProject() !== null ? $request->getProject() : '';
        $resource = "/logstores/$logstore";
        [$resp, $header] = $this->send("GET", $project, NULL, $resource, $params, $headers);
        $requestId = isset ($header ['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return new LogStoreSqlResponse($resp, $header);
    }
    
    /**
     * exeucte project sql.
     * Unsuccessful opertaion will cause an AliyunException.
     *
     * @param ProjectSqlRequest $request the GetLogs request parameters class.
     * @return array(json body, http header)
     * @throws AliyunException
     */
    public function executeProjectSqlJson(ProjectSqlRequest $request)
    {
        $headers = [];
        $params  = [];
        if ($request->getQuery() !== null)
            $params ['query'] = $request->getQuery();
        if ($request->getPowerSql() != null)
            $params ["powerSql"] = $request->getPowerSql() ? 'true' : 'false';
        $project  = $request->getProject() !== null ? $request->getProject() : '';
        $resource = "/logs";
        [$resp, $header] = $this->send("GET", $project, NULL, $resource, $params, $headers);
        $requestId = isset ($header ['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return [$resp, $header];
        //return new GetLogsResponse ( $resp, $header );
    }
    
    /**
     * Get logs from Log service.
     * Unsuccessful opertaion will cause an AliyunException.
     *
     * @param GetProjectLogsRequest $request the GetLogs request parameters class.
     * @return GetLogsResponse
     * @throws AliyunException
     */
    public function executeProjectSql(ProjectSqlRequest $request)
    {
        $ret    = $this->executeProjectSqlJson($request);
        $resp   = $ret[0];
        $header = $ret[1];
        return new ProjectSqlResponse ($resp, $header);
    }
    
    /**
     * create sql instance api
     * Unsuccessful opertaion will cause an AliyunException.
     *
     * @param  $project is project name
     * @param  $cu      is max cores used concurrently in a project
     * @return CreateSqlInstanceResponse
     * @throws AliyunException
     */
    public function createSqlInstance($project, $cu)
    {
        $headers                      = [];
        $params                       = [];
        $resource                     = '/sqlinstance';
        $headers['x-log-bodyrawsize'] = 0;
        $headers ['Content-Type']     = 'application/json';
        $body                         = [
            "cu" => $cu,
        ];
        $body_str                     = json_encode($body);
        [$resp, $header] = $this->send("POST", $project, $body_str, $resource, $params, $headers);
        $requestId = isset ($header ['x-log-requestid']) ?
            $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return new CreateSqlInstanceResponse($resp, $header);
    }
    
    /**
     * update sql instance api
     * Unsuccessful opertaion will cause an AliyunException.
     *
     * @param  $project is project name
     * @param  $cu      is max cores used concurrently in a project
     * @return UpdateSqlInstanceResponse
     * @throws AliyunException
     */
    public function updateSqlInstance($project, $cu)
    {
        $headers                      = [];
        $params                       = [];
        $resource                     = '/sqlinstance';
        $headers['x-log-bodyrawsize'] = 0;
        $headers ['Content-Type']     = 'application/json';
        $body                         = [
            "cu" => $cu,
        ];
        $body_str                     = json_encode($body);
        [$resp, $header] = $this->send("PUT", $project, $body_str, $resource, $params, $headers);
        $requestId = isset ($header ['x-log-requestid']) ?
            $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return new UpdateSqlInstanceResponse($resp, $header);
    }
    
    /**
     * get sql instance api
     * Unsuccessful opertaion will cause an AliyunException.
     *
     * @param  $project is project name
     * @return ListSqlInstanceResponse
     * @throws AliyunException
     */
    public function listSqlInstance($project)
    {
        $headers                    = [];
        $headers['Content-Type']    = 'application/x-protobuf';
        $hangzhou['Content-Length'] = '0';
        $params                     = [];
        $resource                   = '/sqlinstance';
        $body_str                   = "";
        [$resp, $header] = $this->send("GET", $project, $body_str, $resource, $params, $headers);
        $requestId = isset ($header ['x-log-requestid']) ?
            $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return new ListSqlInstanceResponse($resp, $header);
    }
    
    /**
     * Get logs from Log service with shardid conditions.
     * Unsuccessful opertaion will cause an AliyunException.
     *
     * @param BatchGetLogsRequest $request the BatchGetLogs request parameters class.
     * @return BatchGetLogsResponse
     * @throws AliyunException
     */
    public function batchGetLogs(BatchGetLogsRequest $request)
    {
        $params   = [];
        $headers  = [];
        $project  = $request->getProject() !== null ? $request->getProject() : '';
        $logstore = $request->getLogstore() !== null ? $request->getLogstore() : '';
        $shardId  = $request->getShardId() !== null ? $request->getShardId() : '';
        if ($request->getCount() !== null)
            $params['count'] = $request->getCount();
        if ($request->getCursor() !== null)
            $params['cursor'] = $request->getCursor();
        if ($request->getEndCursor() !== null)
            $params['end_cursor'] = $request->getEndCursor();
        $params['type']             = 'log';
        $headers['Accept-Encoding'] = 'gzip';
        $headers['accept']          = 'application/x-protobuf';
        
        $resource = "/logstores/$logstore/shards/$shardId";
        [$resp, $header] = $this->send("GET", $project, NULL, $resource, $params, $headers);
        //$resp is a byteArray
        $resp = gzuncompress($resp);
        if ($resp === false) $resp = new SlsLogGroup();
        
        else {
            $resp = new SlsLogGroup($resp);
        }
        return new BatchGetLogsResponse ($resp, $header);
    }
    
    /**
     * List Shards from Log service with Project and logstore conditions.
     * Unsuccessful opertaion will cause an AliyunException.
     *
     * @param ListShardsRequest $request the ListShards request parameters class.
     * @return ListShardsResponse
     * @throws AliyunException
     */
    public function listShards(ListShardsRequest $request)
    {
        $params   = [];
        $headers  = [];
        $project  = $request->getProject() !== null ? $request->getProject() : '';
        $logstore = $request->getLogstore() !== null ? $request->getLogstore() : '';
        
        $resource = '/logstores/' . $logstore . '/shards';
        [$resp, $header] = $this->send("GET", $project, NULL, $resource, $params, $headers);
        $requestId = isset ($header ['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return new ListShardsResponse ($resp, $header);
    }
    
    /**
     * split a shard into two shards  with Project and logstore and shardId and midHash conditions.
     * Unsuccessful opertaion will cause an AliyunException.
     *
     * @param SplitShardRequest $request the SplitShard request parameters class.
     * @return ListShardsResponse
     * @throws AliyunException
     */
    public function splitShard(SplitShardRequest $request)
    {
        $params   = [];
        $headers  = [];
        $project  = $request->getProject() !== null ? $request->getProject() : '';
        $logstore = $request->getLogstore() !== null ? $request->getLogstore() : '';
        $shardId  = $request->getShardId() !== null ? $request->getShardId() : -1;
        $midHash  = $request->getMidHash() != null ? $request->getMidHash() : "";
        
        $resource         = '/logstores/' . $logstore . '/shards/' . $shardId;
        $params["action"] = "split";
        $params["key"]    = $midHash;
        [$resp, $header] = $this->send("POST", $project, NULL, $resource, $params, $headers);
        $requestId = isset ($header ['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return new ListShardsResponse ($resp, $header);
    }
    
    /**
     * merge two shards into one shard with Project and logstore and shardId and conditions.
     * Unsuccessful opertaion will cause an AliyunException.
     *
     * @param MergeShardsRequest $request the MergeShards request parameters class.
     * @return ListShardsResponse
     * @throws AliyunException
     */
    public function MergeShards(MergeShardsRequest $request)
    {
        $params   = [];
        $headers  = [];
        $project  = $request->getProject() !== null ? $request->getProject() : '';
        $logstore = $request->getLogstore() !== null ? $request->getLogstore() : '';
        $shardId  = $request->getShardId() != null ? $request->getShardId() : -1;
        
        $resource         = '/logstores/' . $logstore . '/shards/' . $shardId;
        $params["action"] = "merge";
        [$resp, $header] = $this->send("POST", $project, NULL, $resource, $params, $headers);
        $requestId = isset ($header ['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return new ListShardsResponse ($resp, $header);
    }
    
    /**
     * delete a read only shard with Project and logstore and shardId conditions.
     * Unsuccessful opertaion will cause an AliyunException.
     *
     * @param DeleteShardRequest $request the DeleteShard request parameters class.
     * @return DeleteShardResponse
     * @throws AliyunException
     */
    public function DeleteShard(DeleteShardRequest $request)
    {
        $params   = [];
        $headers  = [];
        $project  = $request->getProject() !== null ? $request->getProject() : '';
        $logstore = $request->getLogstore() !== null ? $request->getLogstore() : '';
        $shardId  = $request->getShardId() != null ? $request->getShardId() : -1;
        
        $resource = '/logstores/' . $logstore . '/shards/' . $shardId;
        [$resp, $header] = $this->send("DELETE", $project, NULL, $resource, $params, $headers);
        $requestId = isset ($header ['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        return new DeleteShardResponse ($header);
    }
    
    /**
     * Get cursor from Log service.
     * Unsuccessful opertaion will cause an AliyunException.
     *
     * @param GetCursorRequest $request the GetCursor request parameters class.
     * @return GetCursorResponse
     * @throws AliyunException
     */
    public function getCursor(GetCursorRequest $request)
    {
        $params   = [];
        $headers  = [];
        $project  = $request->getProject() !== null ? $request->getProject() : '';
        $logstore = $request->getLogstore() !== null ? $request->getLogstore() : '';
        $shardId  = $request->getShardId() !== null ? $request->getShardId() : '';
        $mode     = $request->getMode() !== null ? $request->getMode() : '';
        $fromTime = $request->getFromTime() !== null ? $request->getFromTime() : -1;
        
        if ((empty($mode) xor $fromTime == -1) == false) {
            if (!empty($mode))
                throw new AliyunException ('RequestError', "Request is failed. Mode and fromTime can not be not empty simultaneously");
            else
                throw new AliyunException ('RequestError', "Request is failed. Mode and fromTime can not be empty simultaneously");
        }
        if (!empty($mode) && strcmp($mode, 'begin') !== 0 && strcmp($mode, 'end') !== 0)
            throw new AliyunException ('RequestError', "Request is failed. Mode value invalid:$mode");
        if ($fromTime !== -1 && (is_integer($fromTime) == false || $fromTime < 0))
            throw new AliyunException ('RequestError', "Request is failed. FromTime value invalid:$fromTime");
        $params['type'] = 'cursor';
        if ($fromTime !== -1) $params['from'] = $fromTime;
        else $params['mode'] = $mode;
        $resource = '/logstores/' . $logstore . '/shards/' . $shardId;
        [$resp, $header] = $this->send("GET", $project, NULL, $resource, $params, $headers);
        $requestId = isset ($header ['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return new GetCursorResponse($resp, $header);
    }
    
    public function createConfig(CreateConfigRequest $request)
    {
        $params  = [];
        $headers = [];
        $body    = null;
        if ($request->getConfig() !== null) {
            $body = json_encode($request->getConfig()->toArray());
        }
        $headers ['Content-Type'] = 'application/json';
        $resource                 = '/configs';
        [$resp, $header] = $this->send("POST", NULL, $body, $resource, $params, $headers);
        return new CreateConfigResponse($header);
    }
    
    public function updateConfig(UpdateConfigRequest $request)
    {
        $params     = [];
        $headers    = [];
        $body       = null;
        $configName = '';
        if ($request->getConfig() !== null) {
            $body       = json_encode($request->getConfig()->toArray());
            $configName = ($request->getConfig()->getConfigName() !== null) ? $request->getConfig()
                ->getConfigName() : '';
        }
        $headers ['Content-Type'] = 'application/json';
        $resource                 = '/configs/' . $configName;
        [$resp, $header] = $this->send("PUT", NULL, $body, $resource, $params, $headers);
        return new UpdateConfigResponse($header);
    }
    
    public function getConfig(GetConfigRequest $request)
    {
        $params  = [];
        $headers = [];
        
        $configName = ($request->getConfigName() !== null) ? $request->getConfigName() : '';
        
        $resource = '/configs/' . $configName;
        [$resp, $header] = $this->send("GET", NULL, NULL, $resource, $params, $headers);
        $requestId = isset ($header ['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return new GetConfigResponse($resp, $header);
    }
    
    public function deleteConfig(DeleteConfigRequest $request)
    {
        $params     = [];
        $headers    = [];
        $configName = ($request->getConfigName() !== null) ? $request->getConfigName() : '';
        $resource   = '/configs/' . $configName;
        [$resp, $header] = $this->send("DELETE", NULL, NULL, $resource, $params, $headers);
        return new DeleteConfigResponse($header);
    }
    
    public function listConfigs(ListConfigsRequest $request)
    {
        $params  = [];
        $headers = [];
        
        if ($request->getConfigName() !== null) $params['configName'] = $request->getConfigName();
        if ($request->getOffset() !== null) $params['offset'] = $request->getOffset();
        if ($request->getSize() !== null) $params['size'] = $request->getSize();
        
        $resource = '/configs';
        [$resp, $header] = $this->send("GET", NULL, NULL, $resource, $params, $headers);
        $requestId = isset ($header ['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return new ListConfigsResponse($resp, $header);
    }
    
    public function createMachineGroup(CreateMachineGroupRequest $request)
    {
        $params  = [];
        $headers = [];
        $body    = null;
        if ($request->getMachineGroup() !== null) {
            $body = json_encode($request->getMachineGroup()->toArray());
        }
        $headers ['Content-Type'] = 'application/json';
        $resource                 = '/machinegroups';
        [$resp, $header] = $this->send("POST", NULL, $body, $resource, $params, $headers);
        
        return new CreateMachineGroupResponse($header);
    }
    
    public function updateMachineGroup(UpdateMachineGroupRequest $request)
    {
        $params    = [];
        $headers   = [];
        $body      = null;
        $groupName = '';
        if ($request->getMachineGroup() !== null) {
            $body      = json_encode($request->getMachineGroup()->toArray());
            $groupName = ($request->getMachineGroup()->getGroupName() !== null) ? $request->getMachineGroup()
                ->getGroupName() : '';
        }
        $headers ['Content-Type'] = 'application/json';
        $resource                 = '/machinegroups/' . $groupName;
        [$resp, $header] = $this->send("PUT", NULL, $body, $resource, $params, $headers);
        return new UpdateMachineGroupResponse($header);
    }
    
    public function getMachineGroup(GetMachineGroupRequest $request)
    {
        $params  = [];
        $headers = [];
        
        $groupName = ($request->getGroupName() !== null) ? $request->getGroupName() : '';
        
        $resource = '/machinegroups/' . $groupName;
        [$resp, $header] = $this->send("GET", NULL, NULL, $resource, $params, $headers);
        $requestId = isset ($header ['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return new GetMachineGroupResponse($resp, $header);
    }
    
    public function deleteMachineGroup(DeleteMachineGroupRequest $request)
    {
        $params  = [];
        $headers = [];
        
        $groupName = ($request->getGroupName() !== null) ? $request->getGroupName() : '';
        $resource  = '/machinegroups/' . $groupName;
        [$resp, $header] = $this->send("DELETE", NULL, NULL, $resource, $params, $headers);
        return new DeleteMachineGroupResponse($header);
    }
    
    public function listMachineGroups(ListMachineGroupsRequest $request)
    {
        $params  = [];
        $headers = [];
        
        if ($request->getGroupName() !== null) $params['groupName'] = $request->getGroupName();
        if ($request->getOffset() !== null) $params['offset'] = $request->getOffset();
        if ($request->getSize() !== null) $params['size'] = $request->getSize();
        
        $resource = '/machinegroups';
        [$resp, $header] = $this->send("GET", NULL, NULL, $resource, $params, $headers);
        $requestId = isset ($header ['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        
        return new ListMachineGroupsResponse($resp, $header);
    }
    
    public function applyConfigToMachineGroup(ApplyConfigToMachineGroupRequest $request)
    {
        $params                   = [];
        $headers                  = [];
        $configName               = $request->getConfigName();
        $groupName                = $request->getGroupName();
        $headers ['Content-Type'] = 'application/json';
        $resource                 = '/machinegroups/' . $groupName . '/configs/' . $configName;
        [$resp, $header] = $this->send("PUT", NULL, NULL, $resource, $params, $headers);
        return new ApplyConfigToMachineGroupResponse($header);
    }
    
    public function removeConfigFromMachineGroup(RemoveConfigFromMachineGroupRequest $request)
    {
        $params                   = [];
        $headers                  = [];
        $configName               = $request->getConfigName();
        $groupName                = $request->getGroupName();
        $headers ['Content-Type'] = 'application/json';
        $resource                 = '/machinegroups/' . $groupName . '/configs/' . $configName;
        [$resp, $header] = $this->send("DELETE", NULL, NULL, $resource, $params, $headers);
        return new RemoveConfigFromMachineGroupResponse($header);
    }
    
    public function getMachine(GetMachineRequest $request)
    {
        $params  = [];
        $headers = [];
        
        $uuid = ($request->getUuid() !== null) ? $request->getUuid() : '';
        
        $resource = '/machines/' . $uuid;
        [$resp, $header] = $this->send("GET", NULL, NULL, $resource, $params, $headers);
        $requestId = isset ($header ['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return new GetMachineResponse($resp, $header);
    }
    
    public function createACL(CreateACLRequest $request)
    {
        $params  = [];
        $headers = [];
        $body    = null;
        if ($request->getAcl() !== null) {
            $body = json_encode($request->getAcl()->toArray());
        }
        $headers ['Content-Type'] = 'application/json';
        $resource                 = '/acls';
        [$resp, $header] = $this->send("POST", NULL, $body, $resource, $params, $headers);
        $requestId = isset ($header ['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return new CreateACLResponse($resp, $header);
    }
    
    public function updateACL(UpdateACLRequest $request)
    {
        $params  = [];
        $headers = [];
        $body    = null;
        $aclId   = '';
        if ($request->getAcl() !== null) {
            $body  = json_encode($request->getAcl()->toArray());
            $aclId = ($request->getAcl()->getAclId() !== null) ? $request->getAcl()->getAclId() : '';
        }
        $headers ['Content-Type'] = 'application/json';
        $resource                 = '/acls/' . $aclId;
        [$resp, $header] = $this->send("PUT", NULL, $body, $resource, $params, $headers);
        return new UpdateACLResponse($header);
    }
    
    public function getACL(GetACLRequest $request)
    {
        $params  = [];
        $headers = [];
        
        $aclId = ($request->getAclId() !== null) ? $request->getAclId() : '';
        
        $resource = '/acls/' . $aclId;
        [$resp, $header] = $this->send("GET", NULL, NULL, $resource, $params, $headers);
        $requestId = isset ($header ['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        
        return new GetACLResponse($resp, $header);
    }
    
    public function deleteACL(DeleteACLRequest $request)
    {
        $params   = [];
        $headers  = [];
        $aclId    = ($request->getAclId() !== null) ? $request->getAclId() : '';
        $resource = '/acls/' . $aclId;
        [$resp, $header] = $this->send("DELETE", NULL, NULL, $resource, $params, $headers);
        return new DeleteACLResponse($header);
    }
    
    public function listACLs(ListACLsRequest $request)
    {
        $params  = [];
        $headers = [];
        if ($request->getPrincipleId() !== null) $params['principleId'] = $request->getPrincipleId();
        if ($request->getOffset() !== null) $params['offset'] = $request->getOffset();
        if ($request->getSize() !== null) $params['size'] = $request->getSize();
        
        $resource = '/acls';
        [$resp, $header] = $this->send("GET", NULL, NULL, $resource, $params, $headers);
        $requestId = isset ($header ['x-log-requestid']) ? $header ['x-log-requestid'] : '';
        $resp      = $this->parseToJson($resp, $requestId);
        return new ListACLsResponse($resp, $header);
    }
    
}

